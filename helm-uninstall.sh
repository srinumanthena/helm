#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "Need an argument to specify the environment (e.g. devc or inta)"
    echo "Need an argument to specify the chart (e.g. backend, bp, pa, or mongo)"
    echo "usage: helm-uninstall.sh <environment> <chart>"
    exit 1
fi
echo "Uninstalling AppSvcs-$2 Helm Chart from the $1 environment"
helm -n $1 uninstall $1-$2  
