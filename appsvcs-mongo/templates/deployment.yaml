{{ range $mongoId, $config := .Values.mongos }}
{{ $numberOfNodes := len $config.nodeLabels }}
{{ $dbType := $config.type }}
{{ $encrypt := $config.encrypt }}
{{ $fipsMode := $config.fipsMode }}
{{- $valid := list "ReplicaSet" "Standalone" }}
{{- if not (has $dbType $valid) }}
  {{- fail "Invalid database type. Should be ReplicaSet or Standalone" }}
{{- end }}
{{- $pvName := print $.Values.environment "-" $config.name }}
{{- $mdbName := print $pvName "-" $dbType }}
{{- if eq $dbType "ReplicaSet" }}
  {{- if lt $numberOfNodes 3 }}
    {{- fail "A ReplicaSet needs at least 3 nodes" }}
  {{- end }}
{{- else if ne $numberOfNodes 1 }}
  {{- fail "A Standalone must have 1 and only 1 node" }} 
{{- end }} 
{{- range $nodeId, $nodeLabel := $config.nodeLabels }}
  {{- $pvPathName := print $pvName }}
  {{- if eq $dbType "ReplicaSet" }}
    {{ $pvPathName = print $pvPathName "-" $nodeId }}
  {{- end }}  
apiVersion: v1
kind: PersistentVolume
metadata:
  name: {{ $pvName }}-mongo-pv-{{ $nodeId }}
  labels:
    app.kubernetes.io/name: {{ $pvName }}-mongo-pv
    app.kubernetes.io/instance: {{ $pvName }}-mongo-pv-{{ $nodeId }}
    app.kubernetes.io/part-of: {{ $config.name }}
    {{- include "appsvcs-mongo.labels" $ | nindent 4 }}
spec:
  capacity:
    storage: {{ $config.storage | default $.Values.defaults.storage | quote }}
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: {{ $.Values.environment }}-mongo-sc
  local:
    path: {{ $config.dataPath | default $.Values.defaults.dataPath }}/{{ $pvPathName }}
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: In
          values:
          - {{ $nodeLabel }}
---
{{- end }}
apiVersion: mongodb.com/v1
kind: MongoDB
metadata:
  name: {{ $mdbName | lower }}
  namespace: {{ $.Release.Namespace }}
  labels:
    app.kubernetes.io/name: {{ $.Values.environment }}-{{ $config.name }}-mongo
    app.kubernetes.io/instance: {{ $.Values.environment }}-{{ $config.name }}-mongo
    app.kubernetes.io/part-of: {{ $config.name }}
    {{- include "appsvcs-mongo.labels" $ | nindent 4 }}
spec:
  featureCompatibilityVersion: {{ $config.compatibilityVersion | default $.Values.defaults.compatibilityVersion  | quote }}
  members: {{ len $config.nodeLabels }}
  version: {{ $.Values.defaults.mongoVersion }}
  type: {{ $config.type | default $.Values.defaults.type }}
  exposedExternally: {{ $config.exposedExternally | default $.Values.defaults.exposedExternally }}
  additionalMongodConfig:
    net:
      ssl:
        mode: allowSSL
      tls:
        FIPSMode: {{ $fipsMode }}
        mode: allowTLS
    security:       
      enableEncryption: {{ $encrypt }}
      {{- if eq $encrypt true }} 
      encryptionKeyFile: /data/keyfiles/mongodb-keyfile
      {{- end }}
  security:
    tls: 
      enabled: {{ $config.tls | default false }}
  {{- if eq $dbType "ReplicaSet" }}
  connectivity:
    replicaSetHorizons:    
    {{- range $horizonId, $horizonSvc := $config.horizonServices }}
      {{- $horizonNameAndPort := print $horizonSvc.name ":" $horizonSvc.nodePort }}
      - {{$mdbName | lower }} : {{ $horizonNameAndPort | quote }} 
    {{- end}}
  {{- end }}
  podSpec:
    podTemplate:
      metadata:
        labels:
          {{- include "appsvcs-mongo.labels" $ | nindent 9 }}
    persistent: {{ $config.persistent | default $.Values.defaults.persistent }}
    persistence:
      single:
        labelSelector: 
          matchLabels:
            app.kubernetes.io/name: {{ $pvName }}-mongo-pv
        storage: {{ $config.storage | default $.Values.defaults.storage }}
        storageClass: {{ $.Values.environment }}-mongo-sc
  opsManager:
    configMapRef:
      name: {{ $.Values.environment}}-ops-manager-connection
  credentials: {{ $.Values.environment }}-ops-manager-credentials
---
  {{- range $horizonId, $horizonSvc := $config.horizonServices }}
  {{- $svcName := print $mdbName "-" $horizonId "-svc-ext" | lower }}
  {{- $podName := print $mdbName "-" $horizonId | lower }}
apiVersion: v1
kind: Service
metadata:
  name: {{ $svcName }}
  namespace: {{ $.Release.Namespace }}
  labels:
    app.kubernetes.io/name: {{ $svcName }}
    app.kubernetes.io/instance: {{ $svcName }}
    app.kubernetes.io/part-of: {{ $config.name }}
    {{- include "appsvcs-mongo.labels" $ | nindent 4 }}
spec:
  ports:
  - port: 27017
    name: "tcp"
    nodePort: {{ $horizonSvc.nodePort }}
  selector:
    statefulset.kubernetes.io/pod-name: {{ $podName }}
  type: NodePort
---
  {{- end}}
{{- end }}
