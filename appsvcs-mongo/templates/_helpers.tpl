{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "appsvcs-mongo.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "appsvcs-mongo.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "appsvcs-mongo.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "appsvcs-mongo.labels" -}}
helm.sh/chart: {{ include "appsvcs-mongo.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/component: {{ .Values.labels.component }}
environment: {{ .Values.environment }}
owner: {{ .Values.labels.orgName }}
stateful: {{ .Values.labels.stateful | quote }}
tier: {{ .Values.labels.tier }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "appsvcs-mongo.selectorLabels" -}}
app.kubernetes.io/name: {{ include "appsvcs-mongo.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "appsvcs-mongo.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "appsvcs-mongo.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create persistent information for the mongodb resource
*/}}
{{- define "appsvcs-mongo.persistence" -}}
{{- if .Values.persistent -}}
persistence:
  single:
    storage: {{ .Values.persistence.storage }}
    storageClass: {{ .Values.persistence.storageClass }}
{{- else -}}
persistent: false
{{- end }}
{{- end }}