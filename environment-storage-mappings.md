# Environment Storage Mappings for Applications Deployed to Kubernetes
This file contains the current mappings of persistent volumes for applications deployed to different environments. An environment for our purposes is equivalent to a Kubernetes namespace. However not all storage for an environment is allocated to that namespace (e.g. mongodb resources).

Storage is local storage as we are not using any kind of storage provisioner. Each environment, though, creates its own storage classes and targets specific nodes. These nodes are labeled with the `tier=database`. See the [Node Designation](https://bitbucket.fssa.in.gov/projects/BPP/repos/kubernetes/browse/redhat7/Node-Designation-Labels.md?at=refs%2Fheads%2Finstall-instructions) page for a listing of those nodes.

All storage nodes store the data under `/var/lib/docker/k8s/volumes`. In the tables below, the Path portion is the path under `volumes`. A __TBD__ indicates that the location has not been determined and should be filled in __PRIOR__ to installing the environment. The complete path __MUST__ exist __PRIOR__ to installing the environment.

## deva environment (To Be Rebuilt)
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [deva-values.yaml](./appsvcs-mongo/deva-values.yaml) file. The storage node information for Keycloak needs to go into the backend [deva-values.yaml](./appsvcs-backend/deva-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |deva-pa-mongo-pv|dfrdkrd22vl|mongodb/deva-pa
|Benefits Portal|mongodb  |deva-bp-mongo-pv|dfrdkrd22vl|mongodb/deva-bp
|Keycloak       |deva     |deva-postgres-pv|dfrdkrd23vl|postgres/deva-keycloak

## devb environment (To Be Built)
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [devb-values.yaml](./appsvcs-mongo/devb-values.yaml) file. The storage node information for Keycloak needs to go into the backend [devb-values.yaml](./appsvcs-backend/devb-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |devb-pa-mongo-pv|TBD        |mongodb/devb-pa
|Benefits Portal|mongodb  |devb-bp-mongo-pv|TBD        |mongodb/devb-bp
|Keycloak       |devb     |devb-postgres-pv|TBD        |postgres/devb-keycloak

## devc environment (To Be Rebuilt)
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [devc-values.yaml](./appsvcs-mongo/devc-values.yaml) file. The storage node information for Keycloak needs to go into the backend [devc-values.yaml](./appsvcs-backend/devc-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |devc-pa-mongo-pv|TBD        |mongodb/devc-pa
|Benefits Portal|mongodb  |devc-bp-mongo-pv|TBD        |mongodb/devc-bp
|Keycloak       |devc     |devc-postgres-pv|dfrdkrd21vl|postgres/devc-keycloak

## sysa environment (To Be Rebuilt)
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [sysa-values.yaml](./appsvcs-mongo/sysa-values.yaml) file. The storage node information for Keycloak needs to go into the backend [sysa-values.yaml](./appsvcs-backend/sysa-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |sysa-pa-mongo-pv|dfrdkrd21vl|mongodb/sysa-pa
|Benefits Portal|mongodb  |sysa-bp-mongo-pv|dfrdkrd21vl|mongodb/sysa-bp
|Keycloak       |sysa     |sysa-postgres-pv|dfrdkrd21vl|postgres/sysa-keycloak

## sysb environment (To Be Built)
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [sysb-values.yaml](./appsvcs-mongo/sysb-values.yaml) file. The storage node information for Keycloak needs to go into the backend [sysb-values.yaml](./appsvcs-backend/sysb-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |sysb-pa-mongo-pv|TBD        |mongodb/sysb-pa
|Benefits Portal|mongodb  |sysb-bp-mongo-pv|TBD        |mongodb/sysb-bp
|Keycloak       |devb     |sysb-postgres-pv|TBD        |postgres/sysb-keycloak

## sysc environment (To Be Built)
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [sysc-values.yaml](./appsvcs-mongo/sysc-values.yaml) file. The storage node information for Keycloak needs to go into the backend [sysc-values.yaml](./appsvcs-backend/sysc-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |sysc-pa-mongo-pv|dfrdkrd31vl|mongodb/sysc-pa
|Benefits Portal|mongodb  |sysc-bp-mongo-pv|dfrdkrd31vl|mongodb/sysc-bp
|Keycloak       |sysc     |sysc-postgres-pv|dfrdkrd21vl|postgres/sysc-keycloak

## inta environment
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [inta-values.yaml](./appsvcs-mongo/inta-values.yaml) file. The storage node information for Keycloak needs to go into the backend [inta-values.yaml](./appsvcs-backend/inta-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |inta-pa-mongo-pv|dfrdkrd23vl|mongodb/inta-pa
|Benefits Portal|mongodb  |inta-bp-mongo-pv|dfrdkrd22vl|mongodb/inta-bp
|Keycloak       |inta     |inta-postgres-pv|dfrdkrd21vl|postgres/inta-keycloak

## uat2 environment
The storage node information for Portal Administration and Benefits Portal needs to go into the mongo [uat2-values.yaml](./appsvcs-mongo/uat2-values.yaml) file. The storage node information for Keycloak needs to go into the backend [uat2-values.yaml](./appsvcs-backend/uat2-values.yaml) file. 

|Application    |Namespace|Persistent Vol. |Node       |Path
|---------------|---------|----------------|-----------|----------
|Portal Admin   |mongodb  |uat2-pa-mongo-pv|dfrdkrd21vl|mongodb/uat2-pa
|Benefits Portal|mongodb  |uat2-bp-mongo-pv|dfrdkrd22vl|mongodb/uat2-bp
|Keycloak       |uat2     |uat2-postgres-pv|dfrdkrd21vl|postgres/uat2-keycloak

