#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "Need an argument to specify the environment (e.g. devc or inta)"
    echo "usage: helm-list.sh <environment>"
    exit 1
fi
helm -n $1 list
