#!/bin/bash
if [ $# -eq 0 ]
  then
    echo "Need an argument to specify the environment (e.g. devc or inta)"
    echo "Need an argument to specify the chart (e.g. backend, bp, pa, or mongo)"
    echo "optionally a --dry-run argument can be specified"
    echo "usage: helm-upgrade.sh <environment> <chart> [--dry-run]"
    exit 1
fi
echo "Installing AppSvcs-$2 Helm Chart to the $1 environment"
helm -n $1 upgrade $1-$2 -f ./appsvcs-$2/$1-values.yaml ./appsvcs-$2 $3 
