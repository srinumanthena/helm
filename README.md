# Helm Charts for Application Services

This repo contains Helm Charts for Application Services apps and components  deployed to Kubernetes. [Helm](https://helm.sh/) is a package manager for kubernetes. A Chart is an application package for Kubernetes.

The Application Services team has created charts for installing and upgrading our applications and their dependent components. This has been done to support installing different versions of our applications into different environments within kubernetes. In our parlance an environment is synonymous with a kubernetes namespace.

## Helm Charts Included for Application Services
- [MongoDB Resources](appsvcs-mongo/README.md)
- [Backend Components](appsvcs-backend/README.md)
- [Benefits Portal](appsvcs-bp/README.md)
- [Portal Administration](appsvcs-pa/README.md)

Please read the instructions for each Chart before deploying them. 

If you are familiar with using Helm and are ready to install a new environment for Application Services the go [here](./environment-installation.md). Otherwise keep reading. 

## Installing, Upgrading, and Uninstalling a Helm Chart
The lifecycle of a Helm Chart includes the initial installation, upgrading the 
chart once it's installed and possibly uninstalling the chart which cleans up the items the Chart manages.

### Install a chart
From the directory that contains the chart (in this case the chart is in a directory) run the helm install command:
```
helm -n deva install deva-bp ./appsvcs-bp
```

Usually its a good idea to perform a "dry run" to make sure that there aren't any issues with the chart. A dry run will process the chart and write the resulting manifests to standard out. If any errors occur they will be outputted as well.
```
helm -n deva install deva-bp ./appsvcs-bp --dry-run
```

You can also provide new variables to override the ones defined in the chart's values.yaml file.
```
helm -n deva install deva-bp -f ./appsvcs-bp/deva-values.yaml ./appsvcs-bp
```
The pieces of the installation command include:
- the target namespace (e.g. -n deva). If not included the currently configured default namespace will be targetted.
- the name for the deployment (e.g. deva-bp). This is the name that will be used to track the chart through its installation and any upgrades.
- the path to the chart (e.g. ./appsvcs-bp). Charts can be packaged into archive files, but in our case they are loosely packed in directories
- flags (e.g. --dry-run) There are other flags that can be optionally used, please read the helm documentation for more info.
- an environment specific values.yaml file (e.g. -f ./appsvcs-bp/deva-values.yaml). There are other ways to provide new values, but this is simplest

### Upgrade a chart
When there are changes made to a chart, helm can be used to upgrade the existing deployed application. To perform an upgrade run the following command:
```
helm -n deva upgrade deva-bp ./appsvcs-bp
```

You can include the --dry-run flag for the upgrade command as well.

### Listing deployed applications
Helm allows you to see applications deployed with Charts, including their current revision (how many times they have been upgraded).
```
helm -n deva list
```

### Uninstalling a Helm Chart
To uninstall an existing chart, simply run the uninstall command ( you do not need to supply a chart directory ).
```
helm -n deva uninstall deva-bp
```

