Thank you for Deploying Application Backend Services to the {{ .Release.Namespace | upper }} environment!

To check the installation you should be able to navigate to the following locations in your browser 

Jhipster Registry: http://dfrdkrd21vl.state.in.us:{{ .Values.registry.nodePort }}/
RabbitMQ Management: http://dfrdkrd21vl.state.in.us:{{ .Values.rabbitmq.managementNodePort }}
Keycloak Console: http://dfrdkrd21vl.state.in.us:{{ .Values.keycloak.nodePort }}
