{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "appsvcs-bp.name" -}}
{{- default $.Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "appsvcs-bp.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "appsvcs-bp.chart" -}}
{{- printf "%s-%s" $.Chart.Name $.Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "appsvcs-bp.labels" -}}
helm.sh/chart: {{ include "appsvcs-bp.chart" $ }}
{{ include "appsvcs-bp.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ $.Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ $.Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "appsvcs-bp.selectorLabels" -}}
app.kubernetes.io/name: {{ include "appsvcs-bp.name" . }}
app.kubernetes.io/instance: {{ $.Release.Name }}
{{- end }}

{{- define "appsvcs-bp.config-labels" -}}
app.kubernetes.io/component: {{ .component }}
app: {{ .name }}
tier: {{ .tier }}
{{- end }}

{{- define "appsvcs-bp.owner-labels" -}}
environment: {{ .Release.Namespace }}
owner: {{ .Values.orgName }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "appsvcs-bp.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "appsvcs-bp.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


