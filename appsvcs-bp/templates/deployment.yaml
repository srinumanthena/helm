{{ range $appId, $config := .Values.applications }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $config.name }}
  namespace: {{ $.Release.Namespace }}
  labels:    
    {{- include "appsvcs-bp.labels" $ | nindent 4 }}
    {{- include "appsvcs-bp.config-labels" $config | nindent 4 }}
    {{- include "appsvcs-bp.owner-labels" $ | nindent 4 }}
spec:
  replicas: {{ $config.replicaCount | default $.Values.defaults.replicaCount }}
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: {{ $config.name }}
  template:
    metadata:
      labels:
        app: {{ $config.name }}
    spec:
      hostname: {{ $config.name }}
      containers:
      - env: 
        - name: EUREKA_CLIENT_SERVICE_URL_DEFAULTZONE
          value: http://admin:${jhipster.registry.password}@jhipster-registry:8761/eureka
        - name: JAVA_OPTS
          value: {{ $config.javaOpts | default $.Values.defaults.javaOpts }}
        - name: JHIPSTER_REGISTRY_PASSWORD
          value: {{ $config.jhipsterRegistryPassword | default $.Values.defaults.jhipsterRegistryPassword | quote }}
        - name: JHIPSTER_SLEEP
          value: {{ $config.jhipsterSleep | default $.Values.defaults.jhipsterSleep | quote }}
        - name: SPRING_CLOUD_CONFIG_LABEL
          value: {{ $config.cloudConfigLabel | default $.Values.defaults.cloudConfigLabel }}
        - name: SPRING_CLOUD_CONFIG_URI
          value: http://admin:${jhipster.registry.password}@jhipster-registry:8761/config
        - name: SPRING_PROFILES_ACTIVE
          value: {{ $config.springProfiles | default $.Values.defaults.springProfiles }}
        - name: SPRING_RABBITMQ_ADDRESSES
          value: {{ $config.rabbitHost | default $.Values.defaults.rabbitHost }}
        - name: SPRING_RABBITMQ_USERNAME
          value: {{ $config.rabbitUsername | default $.Values.defaults.rabbitUsername }}
        - name: SPRING_RABBITMQ_PASSWORD
          value: {{ $config.rabbitPassword | default $.Values.defaults.rabbitPassword }}
        - name: MONGODB_USERNAME
          valueFrom:
            secretKeyRef:
              name: {{ $config.name }}-mongodb
              key: username
              optional: true
        - name: MONGODB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: {{ $config.name }}-mongodb
              key: password
              optional: true        
        - name: spring_profiles_include
          value: swagger 
        - name: APPLICATION_TOKEN_STORE_NODE_ID
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        {{- if $config.hazelCastEnabled }} 
        - name: HAZELCAST_KUBERNETES_NAMESPACE
          value: {{ $.Release.Namespace }}
        - name: HAZELCAST_KUBERNETES_SERVICE_NAME
          value: {{ $config.hazelCastServiceName | default $config.name }}
        {{- end }}
        image: {{ $.Values.defaults.repo }}/{{ $config.name }}/{{ $.Release.Namespace }}:{{ $.Values.defaults.tag }} 
        name: {{ $config.name }}
        imagePullPolicy: {{ $config.pullPolicy | default $.Values.defaults.pullPolicy }}
        ports:
        - containerPort: {{ $config.containerPort }}
        resources:
          requests:
            cpu: {{ $config.cpuRequest | default $.Values.defaults.cpuRequest }}
            memory: {{ $config.memoryRequest | default $.Values.defaults.memoryRequest }}
          limits:
            cpu: {{ $config.cpuLimit | default $.Values.defaults.cpuLimit }}
            memory: {{ $config.memoryLimit | default $.Values.defaults.memoryLimit }}
      restartPolicy: Always
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: tier
                operator: In
                values:
                - frontend
                - application
                - backend
                - both
      initContainers:
      - name: init-{{ $config.name }}-jhipster-registry-check
        image: dfrappd57vl.state.in.us/busybox/busybox:latest
        command: ['sh', '-c', 'until nc -vz jhipster-registry 8761; do echo waiting for jhipster-registry:8761; sleep 2; done;']
---
{{- end }}
