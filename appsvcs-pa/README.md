# Helm Chart for Portal Administration
This Helm Chart deploys the microservices for Portal Administration.
The services include:
- administrationcmd
- administrationqry
- portaladministrationgateway

## Deployment using Helm
Deploying using Helm is done via the command line. In the directory that
contains the appsvcs-pa directory run a command with this structure:

```
helm -n <environment/namespace> install <environment>-pa ./appsvcs-pa --dry-run
```

where
- environment/namespace is the kubernetes namespace. Our approach is that the namespace is the lower case version of the environment.
- --dry-run is an optional argument that does not really deploy the chart but can report any errors in the templates. 

## Prerequisites 
This chart relies on [appsvcs-mongo](../appsvcs-mongo/README.md) and [appsvcs-backend](../appsvcs-backend) and they should be installed for the environment prior to installing this chart.
