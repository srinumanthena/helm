# Environment Port Mappings
This page contains the NodePort mappings for the different services installed to Kubernetes. A NodePort specifies a port on the Kubernetes cluster that allows ingress. So traffic is recieved on this port and then forwarded to the respective pods on their internal ports.

The NodePorts should be specified here __Prior__ to installing a new environment. Then they should be added to the environment's values.yaml file. If an environment is marked (to be built) the values.yaml file will probably not exist.

## deva (To Be Built)
These ports go in the backend [deva-values.yaml](./appsvcs-backend/deva-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |32461
|Keycloak |32480
|Rabbit   |30372, 30375

## devb (To Be Built)
These ports go in the backend [devb-values.yaml](./appsvcs-backend/devb-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |TBD
|Keycloak |TBD
|Rabbit   |TBD, TBD

## devc 
These ports go in the backend [devc-values.yaml](./appsvcs-backend/devc-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |32361
|Keycloak |32360
|Rabbit   |32372, 32375

## sysa (To Be Built)
These ports go in the backend [sysa-values.yaml](./appsvcs-backend/sysa-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |32561
|Keycloak |32580
|Rabbit   |30472, 30973

## sysb (To Be Built)
These ports go in the backend [sysb-values.yaml](./appsvcs-backend/sysb-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |TBD
|Keycloak |TBD
|Rabbit   |TBD, TBD

## sysc (To Be Built)
These ports go in the backend [sysc-values.yaml](./appsvcs-backend/sysc-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |32661
|Keycloak |32680
|Rabbit   |30672, 32973

## inta
These ports go in the backend [inta-values.yaml](./appsvcs-backend/inta-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |30561 
|Keycloak |30560
|Rabbit   |30572, 30575

## uat2
These ports go in the backend [uat2-values.yaml](./appsvcs-backend/uat2-values.yaml) file.

| Service | Node Port(s)
|---------|---------------
|Registry |30661 
|Keycloak |30660
|Rabbit   |30672, 30675