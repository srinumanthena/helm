# Installing a new environment using Helm
This is a set of instructions for installing a new environment into a
kubernetes cluster. It covers the installation of Benefits Portal, 
Portal Administration, the backend services, and MongoDB resources.

**Note:** References to {env} means the environment name (e.g. deva, inta, uat2)

When running the helm commands, it is assumed that they will be run from 
`/root/helm` on the master node __dfrdkrd20vl__.

## Prerequisites
- Confirm that the desired images are in [Harbor](https://dfrappd57vl.state.in.us/harbor) and tagged to the environment.
  + This is extremely important with MR72. MR72 has changes to event processing and if an MR72 image or later is not used, there will be issues as existing events will be reprocessed. So __check__ and then __double-check__.
- Determine the storage locations for the new environment.
  + Update the [Storage Mappings](./environment-storage-mappings.md). 
    * There should already be sections for most environments. Each section that needs to be built or rebuilt will have to be updated. If the new environment does not have a section, create it.
- Determine the port mappings for the new environment
  + Update the [Port Mappings](./environment-port-mappings.md)
    * There should already by sections for most environments. Each section that needs to be built or rebuilt will have to be updated. If the new environment does not have a section, create it.
- Create a namespace for the new environment __all lower case__ 
``` 
  kubectl create ns {env}
```

## Install the Mongo Chart
First, install the [Mongo chart](./appsvcs-mongo/README.md). 

### Create Mongo Data Directories
Using the information in the [Storage Mappings](./environment-storage-mappings.md), create the directories to hold the persistent data.

#### Create Mongo Data Directory for Benefits Portal
- Log onto the node that will be used to hold the mongo database for Benefits Portal. (e.g dfrdkrd21vl.state.in.us and should be specified in the Storage Mapping file)
- Create the directory as shown below, replacing {env} with the environment name (e.g. deva)
```
  mkdir /var/lib/docker/k8s/volumes/mongodb/{env}-bp
  chown -R 2000:2000 /var/lib/docker/k8s/volumes/mongodb/{env}-bp
  chmod -R 770 /var/lib/docker/k8s/volumes/mongodb/{env}-bp
```

#### Create Mongo Data Directory for Portal Administration
- Log onto the node that will be used to hold the mongo database for Portal Administration. (e.g dfrdkrd21vl.state.in.us and should be specified in the Storage Mapping file)
- Create the directory as shown below, replacing {env} with the environment name (e.g. deva)
```
  mkdir /var/lib/docker/k8s/volumes/mongodb/{env}-pa
  chown -R 2000:2000 /var/lib/docker/k8s/volumes/mongodb/{env}-pa
  chmod -R 770 /var/lib/docker/k8s/volumes/mongodb/{env}-pa
```

### Create environment values YAML file in Mongo Chart
In the appsvcs-mongo chart, create a file called {env}-values.yaml. This file will hold the overrides specified in the values.yaml file. The structure should look like this:
```
# Specific Mongo values for the {env} environment

environment: {env}

mongos:
  - name: bp    
    dataPath: /var/lib/docker/k8s/volumes/mongodb
    nodeLabel: dfrdkrdxxvl.state.in.us # Node where data will be stored
    type: Standalone
  - name: pa    
    dataPath: /var/lib/docker/k8s/volumes/mongodb
    nodeLabel: dfrdkrdxxvl.state.in.us # Node where data will be stored
    type: Standalone
```

Replace the {env} with the name of the new environment (e.g. inta) and the nodeLabels with the nodes that will hold the data. __Note__ the pa and bp section can have different nodeLabel designations. The nodeLabel must match the node where the pa and bp directories were created.

Once the environment YAML files are created check them in.

### Perform the Helm Install of the Mongo Chart

```
  helm -n mongodb install {env}-mongo -f appsvcs-mongo/{env}-values.yaml appsvcs-mongo
```

### Verify Mongo is running
Once the chart is installed, verify that the pods are running. `kubectl -n mongodb get pods`. Then verify that there are 4 new services, 2 for bp and 2 for pa:
```
kubectl -n mongodb get svc | grep {env}-
```
You should see something like this:
```
[root@dfrdkrd20vl ~]# kubectl -n mongodb get svc | grep inta-
inta-bp-standalone-svc              ClusterIP   None             <none>        27017/TCP         6d22h
inta-bp-standalone-svc-external     NodePort    10.107.166.153   <none>        27017:31466/TCP   6d22h
inta-pa-standalone-svc              ClusterIP   None             <none>        27017/TCP         6d22h
inta-pa-standalone-svc-external     NodePort    10.110.150.205   <none>        27017:31873/TCP   6d22h
```
The NodePort services ending in '-external' can be used to connect to the mongo instance from outside the Cluster (e.g. with Studio 3T). You just need the exposed port number (i.e. 31873 for PA or 31466 for BP). The other svcs are used by applications running within the cluster to connect to the Mongo instance. If connecting from another namespace, which it will be, the service url has to include the mongodb namespace at the end:
```
mongodb://{env}-bp-standalone-svc.mongodb
```
You do not need to specify port 27017 since it is the default port.

### Scale down Mongo and copy data
In order for the data to be copied from an existing environment into the new one, Mongo needs to be shut down.
```
kubectl -n mongodb scale sts {env}-bp-standalone --replicas=0
kubectl -n mongodb scale sts {env}-pa-standalone --replicas=0
```
Notify the dba that the data can be copied now into the new data directories.

### Scale up Mongo
Once the data is copied, scale Mongo back up.
```
  kubectl -n mongodb scale sts {env}-bp-standalone --replicas=1
  kubectl -n mongodb scale sts {env}-pa-standalone --replicas=1
```

You can use the external NodePort services to login and check that the data looks ok. If all looks good then proceed with the next steps.

## Install the Backend Chart
Next install the [Backend chart](./appsvcs-backend/README.md) that contains services used by the application. These will be installed into the environment namespace. 

### Create a postgres database directory on a stateful node
- Log onto the node that will be used to hold the postgres database for Keycloak. (e.g dfrdkrd21vl.state.in.us and should be specified in the Storage Mapping file)
- Create the directory as shown below, replacing {env} with the environment name (e.g. deva)
  ```
  mkdir /var/lib/docker/k8s/volumes/postgres/{env}-keycloak
  chmod 777 /var/lib/docker/k8s/volumes/postgres/{env}-keycloak
  ```

### Create environment values YAML file in Backend Chart
In the appsvcs-backend chart, create a file called {env}-values.yaml. This file will hold the overrides specified in the values.yaml file. The structure should look like this:
```
# Specific values for the {env} environment

postgres:  
  node: dfrdkrd21vl.state.in.us # Node where data will be stored 

keycloak:  
  nodePort: 30560  

registry:
  nodePort: 30561  
  cloudConfigLabel: {env} # May need to be uppercased if existing in bitbucket

rabbitmq: 
  managementNodePort: 30572
  amqNodePort: 30575
```

Replace the {env} with the name of the new environment (e.g. inta) and the node entry with the node that will hold the data. Replace the nodePort numbers with the ports from the [Port Mappings](./environment-port-mappings.md) file for the environment.

### Perform Helm Install of the Backend Chart
Once the {env}-values.yaml file has been created, it's time to install the 
chart. The [helper script](./helm-install.sh) can be used for this task as it saves some typing and hopefully some confusion.
```
./helm-install.sh {env} backend
```

### Verify Installation
Once the pods have started `kubectl -n {env} get pods`, verify that the postgres data directory contains actual data files. It should look something like:
```
[root@dfrdkrd21vl ~]# ls /var/lib/docker/k8s/volumes/postgres/inta-keycloak/pgdata/
base    pg_commit_ts  pg_hba.conf    pg_logical    pg_notify    pg_serial     pg_stat      pg_subtrans  pg_twophase  pg_wal   postgresql.auto.conf  postmaster.opts
global  pg_dynshmem   pg_ident.conf  pg_multixact  pg_replslot  pg_snapshots  pg_stat_tmp  pg_tblspc    PG_VERSION   pg_xact  postgresql.conf       postmaster.pid
```

The __pgdata__ directory is created by postgres and contains the data files.

### Setup Keycloak
Keycloak should be running and now it can be configured for the needed realms.
- Import Json Files for PA and BP
- Create tokens for service accounts

### Verify JHipster Registry is available
Verify that the JHipster registry is available on its NodePort and pulling data from bitbucket.

### Verify RabbitMQ is available
Verify that the RabbitMQ Management web app is available on its NodePort.

## Install Benefits Portal Chart
Now its time to install Benefits Portal.

### Update Property files
Need to update the property files for the services that make up Benefits Portal. The following changes need to be made:
- application.yml - Update Keycloak uri and service tokens
- The 'cmd' and 'qry' services - Change MongoURI variables to use the new internal mongo service {e.g. inta-bp-standalone-svc.mongodb}

### Create environment YAML files for Benefits Portal Chart
In the appsvcs-bp chart, create a file called {env}-values.yaml. This file will hold the overrides specified in the values.yaml file. The structure should look like this:
```
# This file provides overrides to the existing values.yaml file

defaults:
  cloudConfigLabel: {env} # Maybe uppercased needs to match bitbucket label
  fqdn: benefitsinta.fssa.in.gov
```
The fqdn will need to match the new environment.

### Perform Helm Install of Benefits Portal Chart
Once the {env}-values.yaml file has been created, it's time to install the 
chart. The [helper script](./helm-install.sh) can be used for this task as it saves some typing and hopefully some confusion.
```
./helm-install.sh {env} bp
```

### Update the webserver
A webserver sits in front of the cluster and forwards traffic to the cluster. 
Now is the time to change the configuration of the WebServer to forward traffic matching the fqdn to the kubernetes cluster.

### Verify Benefits Portal
Once the webserver has been configured and restarted and the pods are running, you can test opening the Benefits Portal web page and create a snap application.


## Install Portal Administration Chart
Now its time to install Portal Administration.

### Update Property files
Need to update the property files for the services that make up Portal Administration. The following changes need to be made:
- application.yml - Update Keycloak uri and service tokens
- The 'cmd' and 'qry' and gateway services - Change MongoURI variables to use the new internal mongo service {e.g. inta-pa-standalone-svc.mongodb}

### Create environment YAML files for Portal Administration Chart
In the appsvcs-pa chart, create a file called {env}-values.yaml. This file will hold the overrides specified in the values.yaml file. The structure should look like this:
```
# This file provides overrides to the existing values.yaml file

defaults:
  cloudConfigLabel: {env} # Maybe uppercased needs to match bitbucket label
  fqdn: portaladmininta.fssa.in.gov
```
The fqdn will need to match the new environment.

### Perform Helm Install of Portal Administration Chart
Once the {env}-values.yaml file has been created, it's time to install the 
chart. The [helper script](./helm-install.sh) can be used for this task as it saves some typing and hopefully some confusion.
```
./helm-install.sh {env} pa
```

### Update the webserver
A webserver sits in front of the cluster and forwards traffic to the cluster. 
Now is the time to change the configuration of the WebServer to forward traffic matching the fqdn to the kubernetes cluster.

### Verify Portal Administration
Once the webserver has been configured and restarted and the pods are running, you can test opening the Portal Administration web page and log in.

### Update Hazelcast service account
``` 
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: default-cluster
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: view
subjects:
- kind: ServiceAccount
  name: default
  namespace: deva
- kind: ServiceAccount
  name: default
  namespace: sysa
- kind: ServiceAccount
  name: default
  namespace: inta
- kind: ServiceAccount
  name: default
  namespace: uatb
- kind: ServiceAccount
  name: default
  namespace: sysc
- kind: ServiceAccount
  name: default
  namespace: devc
```